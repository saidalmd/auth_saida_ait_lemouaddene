<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Mail\ConfirmMail;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class AuthentificationController extends Controller
{
    //
    // fonction qui affiche le formulaire de connexion
    public function index(){
        if(Session::has('info')){
            return view('auth.dashboard');
        }
        else{
            return view('auth.index');
        }
    }


    // fonction qui traite les données de connexion et renvoie au dashboard
    public function login(Request $request){
        $introuvable=false;
        $wrong_password=false;
        $user=User::where('email',$request->input('email'))
                    ->first();
        if($user){
            if(password_verify($request->input('password'),$user->password)){
            $info=[
                'id'=>$user->id,
                'name'=>$user->name,
                'email'=>$user->email,
            ];
            $request->session()->put('info',$info);
            $name=$user->name;
            return view('auth.dashboard',compact('name'));
            }
            else {
                $wrong_password=true;
                return view('auth.index',compact('wrong_password'));

            }
        }
        else{
            $introuvable=true;
            return view('auth.index',compact('introuvable'));
        }
    }


    // fonction qui déconnecte 
    public function logout(){
        session()->forget('info');
        return redirect()->route('login-index');
    }

    // fonction qui affiche un formulaire pour ajouter un nouveau membre
    public function form_register(){
        return view('auth.formRegister');
    }

    // fonction qui ajoute un membre à la base de données
    public function register(Request $request){
        $request->validate([
            'name'=>'required|string',
            'email'=>'required|unique:users,email',
            'password'=>'required|confirmed'
        ]);
        $dataTemp=[
            'name'=>$request->input('name'),
            'email'=>$request->input('email'),
            'password'=>password_hash($request->input('password'),PASSWORD_DEFAULT),
            'token_confirmation'=>Str::random(30),
        ];
        $request->session()->put('dataTemp',$dataTemp);

        Mail::to($request->input('email'))->send(new ConfirmMail($request->session()->get('dataTemp.token_confirmation')) );

        $confirmation=true;
        return view('auth.formRegister',compact('confirmation'));
    }

    // fonction qui verifie les infos du nouveau user
    public function confirm_mail($token){
        if($token!=session()->get('dataTemp.token_confirmation')){
            $falsetoken=true;
            return view('auth.formRegister',compact('falsetoken')) ;
        }
        $user=User::create([
            'name'=>session()->get('dataTemp.name'),
            'email'=>session()->get('dataTemp.email'),
            'password'=>session()->get('dataTemp.password'),
            'email_verified_at'=>now()
        ]);
        $success=true;
        $email=session()->get('dataTemp.email');
        return view('auth.index',compact('success','email'));
    }

    // fonction qui affiche le formulaire de reset password
    public function form_reset(){
        return view('auth.formReset');
    }

    // fonction qui permet de changer le mot de passe en cas d'oubli
    public function reset_password(Request $request){
        $user=User::where('email',$request->input('email'))->first();
        
        if($user){
            if($request->input('password')==$request->input('confirmation_password')){
                $user->update([
                    "password"=>password_hash($request->input('password'),PASSWORD_DEFAULT)
                ]);
                $reset=true;
                $email=$request->input('email');

                return view('auth.index',compact('reset','email'));
            }
            else {
                $unmatch=true;
                return view('auth.formReset',compact('unmatch'));
            }
        }
        else {
            $notFound=true;
            return view('auth.formReset',compact('notFound'));
        }





    }
}
