let form=document.getElementsByTagName("form")[0];

let emailReg = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
let passwordReg=/^[a-zA-Z0-9]{8,}$/;

let err_email=document.getElementById("err_email");
let err_password=document.getElementById("err_password");
let err_confirm_password=document.getElementById("err_confirm_password");

form.addEventListener("submit",function(event){
    let email=document.getElementById("email").value;
    let password=document.getElementById("password").value;
    let confirm_password=document.getElementById("confirm_password").value;

    if(!emailReg.test(email)){
        err_email.textContent="L'adresse email fourni n'est pas valide";
        event.preventDefault();

    }
    if(!passwordReg.test(password)){
        err_password.textContent="Le mot de passe saisi est court, doit inclure minimum 8 caractères";
        event.preventDefault();

    }
    if(confirm_password!=password){
        err_confirm_password.innerText="La confirmation du mot de passe est fausse";
        event.preventDefault();

    }

    
})
