<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/dashboard.css') }}">
    <title>Document</title>
</head>
<body>
    <h3>
        Welcome
        @if (isset($name))
            <span> {{$name}} </span>
        @else
        <span> {{session()->get('info.name')}} </span>
        @endif
    </h3>
    <a href="{{route('logout')}}">logout</a>
    <a href="#">profile</a>
    
</body>
</html>