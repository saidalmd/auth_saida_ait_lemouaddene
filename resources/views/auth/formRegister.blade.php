@extends('layouts.user')
@section('content')
    <main>
        <section>
            <form action="{{route('register')}}" method="post">
                @csrf
                <div>
                    <label for="name">Name</label>
                    <input type="text" id="name" name="name">
                </div>
                <div>
                    <label for="email">Email</label>
                    <input type="email" name="email" id="email" required>
                    <span id="err_email"></span>
                </div>
                <div>
                    <label for="password">Password</label>
                    <input type="password" id="password" name="password" required> 
                    <span id="err_password"></span>
                </div>
                <div>
                    <label for="confirm_password">Confirm password</label>
                    <input type="password" id="confirm_password" name="password_confirmation" required>
                    <span id="err_confirm_password"></span>
                </div>
                <div>
                    <input type="submit" value="Register" required>
                </div>
            </form>
        </section>
    @if (isset($falsetoken))
        <p>The confirmation has failed.</p>
    @endif

    @if (isset($confirmation))
        <span>A link confirmation has been sent to your account, please check it.</span>
    @endif

    </main>
    <script src="{{ asset('js/form_register.js') }}"></script>
    
@endsection