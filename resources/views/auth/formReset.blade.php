@extends('layouts.user')
@section('content')
    <main>
        <section>
            <form action="{{route('reset_password')}}" method="post">
                @csrf
                <div>
                    <label for="email">Email</label>
                    <input type="email" id="email" name="email" value="{{old('email')}}">
                    @if (isset($notFound))
                        <p>This user doesn't exist!</p>
                    @endif
                </div>
                <div>
                    <label for="password">New password</label>
                    <input type="password" id="password" name="password" value="{{old('password')}}">
                </div>
                <div>
                    <label for="confirmation_password">Confirm password</label>
                    <input type="password" id="confirmation_password" name="confirmation_password" value="{{old('confirmation_password')}}">
                    @if (isset($unmatch))
                        <p>The confirmation doesn't match, please try again!</p>
                    @endif
                </div>
                <div>
                    <input type="submit" value="Confirm">
                </div>
            </form>
        </section>    
    </main>
@endsection