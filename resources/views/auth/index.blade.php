@extends('layouts.user')
@section('content')
    <main>
    <section>
        <form action="{{route('login')}}" method="post">
            @csrf
            <div>
                <label for="email">Email</label>
                <input type="email" id="email" name="email" required value= @if (isset($email))
                    {{$email}}
                @endif >
                @if (isset($introuvable))
                    <p>This user doesn't exist!</p>
                @endif
            </div>
            <div>
                <label for="password">Password</label>
                <input type="password" id="password" name="password" required>
                @if (isset($wrong_password))
                    <p>The password is incorrect</p>    
                @endif
            </div>
            <div>
                <input type="submit" value="Login">
            </div>
        </form>
        <div>
            {{-- <a href="{{route('form_register')}}">Register</a> --}}
            <a href="{{route('form_reset')}}">Forget password?</a>
        </div>
    </section>

        @if (isset($success))
            <span>Your account has been created succefully</span>
        @endif

        @if (isset($reset))
            <span>Your password is updated</span>
        @endif
    </main>
       

@endsection