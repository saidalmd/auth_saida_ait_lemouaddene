<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthentificationController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/index',[AuthentificationController::class,'index'])->name('login-index');
Route::post('/login',[AuthentificationController::class,'login'])->name('login');
Route::get('/logout',[AuthentificationController::class,'logout'])->name('logout');
Route::get('/form_register',[AuthentificationController::class,'form_register'])->name('form_register');
Route::post('/register',[AuthentificationController::class,'register'])->name('register');
Route::get('/confirm_mail/{token}',[AuthentificationController::class,'confirm_mail'])->name('confirm_mail');
Route::get('/form_reset',[AuthentificationController::class,'form_reset'])->name('form_reset');
Route::post('/reset_password',[AuthentificationController::class,'reset_password'])->name('reset_password');